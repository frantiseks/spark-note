/*
 * 
 */
package eu.devs.spark.note.document.codec;

import eu.devs.spark.note.document.UserDocument;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

/**
 * Serialization codec for {@link UserDocument}.
 *
 * @author František Špaček
 */
public class UserCodec extends BaseCodec<UserDocument> {

    @Override
    public void encode(BsonWriter writer, UserDocument value,
            EncoderContext encoderContext) {
        Document document = new Document();
        document.append("_id", value.getId())
                .append("username", value.getUsername())
                .append("password", value.getPasswordHash())
                .append("firstname", value.getFirstName())
                .append("lastname", value.getLastName())
                .append("email", value.getEmail());

        documentCodec.encode(writer, document, encoderContext);
    }

    @Override
    public Class<UserDocument> getEncoderClass() {
        return UserDocument.class;
    }

    @Override
    public UserDocument decode(BsonReader reader,
            DecoderContext decoderContext) {
        Document document = documentCodec.decode(reader, decoderContext);
        UserDocument user = new UserDocument();
        user.setId(document.getString("_id"));
        user.setUsername(document.getString("username"));
        user.setFirstName(document.getString("firstname"));
        user.setLastName(document.getString("lastname"));
        user.setEmail(document.getString("email"));
        user.setPasswordHash(document.getString("password"));

        return user;

    }

}
