/*
 * 
 */
package eu.devs.spark.note.document.codec;

import eu.devs.spark.note.document.BaseDocument;
import java.util.UUID;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.codecs.CollectibleCodec;
import org.bson.codecs.DocumentCodec;

/**
 * Base implementation of {@link CollectibleCodec}.
 *
 * @author František Špaček
 * @param <T> type of document
 */
public abstract class BaseCodec<T extends BaseDocument>
        implements CollectibleCodec<T> {

    protected final CollectibleCodec<Document> documentCodec;

    public BaseCodec() {
        this.documentCodec = new DocumentCodec();
    }
            
    @Override
    public T generateIdIfAbsentFromDocument(T document) {
        document.setId(UUID.randomUUID().toString());
        return document;
    }

    @Override
    public boolean documentHasId(T document) {
        return document.getId() != null;
    }

    @Override
    public BsonValue getDocumentId(T document) {
        return new BsonString(document.getId());
    }

}
