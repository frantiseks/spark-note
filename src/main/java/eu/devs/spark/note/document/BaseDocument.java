/*
 * 
 */
package eu.devs.spark.note.document;

import java.util.Objects;

/**
 * Base document model.
 *
 * @author František Špaček
 */
public abstract class BaseDocument {

    protected String id;

    public BaseDocument() {
    }

    public BaseDocument(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BaseDocument other = (BaseDocument) obj;
        return Objects.equals(this.id, other.id);
    }

}
