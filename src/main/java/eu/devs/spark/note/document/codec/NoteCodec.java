/*
 * 
 */
package eu.devs.spark.note.document.codec;

import eu.devs.spark.note.document.NoteDocument;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

/**
 * Serialization codec for {@link NoteDocument}.
 *
 * @author František Špaček
 */
public class NoteCodec extends BaseCodec<NoteDocument> {
    
    @Override
    public void encode(BsonWriter writer, NoteDocument value,
            EncoderContext encoderContext) {
        Document document = new Document();
        document.append("_id", value.getId())
                .append("title", value.getTitle())
                .append("content", value.getContent())
                .append("created", value.getCreated())
                .append("edited", value.getEdited())
                .append("userId", value.getUserId());
        documentCodec.encode(writer, document, encoderContext);
    }
    
    @Override
    public Class<NoteDocument> getEncoderClass() {
        return NoteDocument.class;
    }
    
    @Override
    public NoteDocument decode(BsonReader reader,
            DecoderContext decoderContext) {
        Document document = documentCodec.decode(reader, decoderContext);
        
        NoteDocument note = new NoteDocument();
        note.setId(document.getString("_id"));
        note.setTitle(document.getString("title"));
        note.setContent(document.getString("content"));
        note.setCreated(document.getDate("created"));
        note.setEdited(document.getDate("edited"));
        note.setUserId(document.getString("userId"));
        return note;
    }
    
}
