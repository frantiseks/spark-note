/*
 * 
 */
package eu.devs.spark.note.document;

import java.util.Date;

/**
 * Document representation of note.
 *
 * @author František Špaček
 */
public class NoteDocument extends BaseDocument {

    private String title;
    private String content;
    private Date created;
    private Date edited;
    private String userId;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getEdited() {
        return edited;
    }

    public void setEdited(Date edited) {
        this.edited = edited;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
