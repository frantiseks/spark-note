/*
 */
package eu.devs.spark.note.auth;

import eu.devs.spark.note.api.dto.RegisterDto;
import eu.devs.spark.note.exception.InternalServerErrorException;
import eu.devs.spark.note.exception.InvalidCredentialsException;
import eu.devs.spark.note.exception.InvalidInputException;

/**
 * Provides authentication for the REST endpoints.
 *
 * @author František Špaček
 */
public interface Authenticator {

    /**
     * Exchanges user credentials for stateless token.
     *
     * @param username username
     * @param password password
     * @return stateless token, never {@code null}
     * @throws IllegalArgumentException if username or password is {@code null}
     * @throws InternalServerErrorException
     * @throws InvalidCredentialsException if provided username or password are
     * invalid.
     */
    String getTokenFor(String username, String password)
            throws InvalidCredentialsException, InternalServerErrorException;

    /**
     * Checks if provided token is valid and authenticated to requested operation
     *
     * @param token stateless token with user data
     * @return user id if authenticated otherwise {@code null}
     * @throws InternalServerErrorException
     */
    String isAuthenticated(String token) throws InternalServerErrorException;

    /**
     * Registers new user to the system.
     *
     * @param data user registration data
     * @throws InvalidInputException if data are invalid or if username or mail
     * is already registered.
     * @throws InternalServerErrorException
     */
    void registerUser(RegisterDto data) throws InvalidInputException,
            InternalServerErrorException;
}
