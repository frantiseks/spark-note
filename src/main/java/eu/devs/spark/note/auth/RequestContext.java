/*
 * 
 */

package eu.devs.spark.note.auth;

import eu.devs.spark.note.util.AssertUtil;

/**
 * Request context.
 *
 * @author František Špaček
 */
public class RequestContext {

  private static final ThreadLocal<RequestContext> CONTEXT = new ThreadLocal<>();

  private String uid;

  public static RequestContext get() {
    if (CONTEXT.get() == null) {
      CONTEXT.set(new RequestContext());
    }
    return CONTEXT.get();
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = AssertUtil.checkNotEmpty(uid, "uid");
  }

  public static void clear() {
    CONTEXT.remove();
  }

}
