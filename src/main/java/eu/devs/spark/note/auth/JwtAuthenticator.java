/*
 */
package eu.devs.spark.note.auth;

import eu.devs.spark.note.api.dto.RegisterDto;
import eu.devs.spark.note.document.UserDocument;
import eu.devs.spark.note.exception.InternalServerErrorException;
import eu.devs.spark.note.exception.InvalidAuthenticationException;
import eu.devs.spark.note.exception.InvalidCredentialsException;
import eu.devs.spark.note.exception.InvalidInputException;
import eu.devs.spark.note.service.UserService;
import eu.devs.spark.note.util.AssertUtil;
import eu.devs.spark.note.util.DigestUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT token Authenticator.
 *
 * @author František Špaček
 */
public class JwtAuthenticator implements Authenticator {

    private static final int TOKEN_EXPIRATION = 60 * 60 * 1000;
    private final UserService userService;
    private final String signingKey;

    /**
     * Creates instance for provided service.
     *
     * @param userService instance of user service
     * @param signingKey base64 signing key
     * @throws IllegalArgumentException if provided argument is {@code null}.
     */
    public JwtAuthenticator(UserService userService, String signingKey) {
        this.userService = AssertUtil.checkNotNull(userService, "userService");
        this.signingKey = AssertUtil.checkNotEmpty(signingKey, "signingKey");
    }

    @Override
    public String getTokenFor(String username, String password)
            throws InvalidCredentialsException, InternalServerErrorException {
        String userId = userService.getUserId(username, password);
        if (userId == null || userId.isEmpty()) {
            throw new InvalidCredentialsException("Invalid username or "
                    + "password");
        }
        return jwtToken(userId);
    }

    @Override
    public String isAuthenticated(String token)
            throws InvalidAuthenticationException,
            InternalServerErrorException {
        final Jws<Claims> claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(signingKey)
                    .parseClaimsJws(token);
        } catch (SignatureException | ExpiredJwtException ex) {
            throw new InvalidAuthenticationException("invalid authentication "
                    + "data", ex);
        }
        String uid = claims.getBody().get("uid", String.class);
        return userService.isValid(uid);
    }

    @Override
    public void registerUser(RegisterDto data) throws InvalidInputException,
            InternalServerErrorException {
        UserDocument user = new UserDocument();
        user.setUsername(data.getUsername());
        user.setFirstName(data.getFirstname());
        user.setLastName(data.getLastname());
        user.setPasswordHash(DigestUtil.sha256(data.getPassword()));
        user.setEmail(data.getEmail());
        userService.registerUser(user);
    }

    private String jwtToken(String uid) {
        final Map<String, Object> claims = new HashMap<>();
        claims.put("uid", uid);

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(getExpiration())
                .signWith(SignatureAlgorithm.HS256, signingKey)
                .compact();
    }

    private Date getExpiration() {
        return new Date(System.currentTimeMillis() + TOKEN_EXPIRATION);
    }
}
