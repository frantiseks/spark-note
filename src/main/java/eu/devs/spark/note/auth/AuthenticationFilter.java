/*
 * 
 */
package eu.devs.spark.note.auth;

import static spark.Spark.before;
import static spark.Spark.halt;

import eu.devs.spark.note.enums.HttpCode;
import eu.devs.spark.note.exception.InternalServerErrorException;
import eu.devs.spark.note.exception.InvalidAuthenticationException;
import eu.devs.spark.note.util.AssertUtil;
import spark.Request;

import static spark.Spark.before;
import static spark.Spark.halt;

/**
 * Authentication filter for API endpoints.
 *
 * @author František Špaček
 */
public class AuthenticationFilter {

  private static final String TOKEN_PREFIX = "Bearer ";
  private static final String TOKEN_HEADER = "Authorization";
  private final Authenticator authenticator;

  public AuthenticationFilter(Authenticator authenticator) {
    this.authenticator = AssertUtil.checkNotNull(authenticator,
        "authenticator");
    setupFilter();
  }

  private void setupFilter() {
    before("/api/*", (req, resp) -> checkAuthentication(req));
  }

  private void checkAuthentication(Request req)
      throws InvalidAuthenticationException, InternalServerErrorException {
    String token = req.headers(TOKEN_HEADER);

    if (token == null || token.isEmpty()) {
      throw new InvalidAuthenticationException("token must be non-empty "
          + "string");
    }

    String uid = authenticator.isAuthenticated(token.replace(TOKEN_PREFIX, ""));

    if (uid == null) {
      halt(HttpCode.UNAUTHORIZED.getCode());
    }

    RequestContext.get().setUid(uid);
  }

}
