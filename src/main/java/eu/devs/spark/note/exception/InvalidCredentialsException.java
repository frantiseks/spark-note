/*
 * 
 */
package eu.devs.spark.note.exception;

/**
 * Exception for invalid authentication credentials.
 *
 * @author František Špaček
 */
public class InvalidCredentialsException extends RuntimeException {

    public InvalidCredentialsException(String message) {
        super(message);
    }

    public InvalidCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }
}
