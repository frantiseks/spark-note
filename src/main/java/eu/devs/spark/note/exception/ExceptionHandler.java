/*
 * 
 */
package eu.devs.spark.note.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.devs.spark.note.enums.ContentType;
import eu.devs.spark.note.enums.HttpCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Response;
import static spark.Spark.exception;

/**
 * Global exception handler.
 *
 * @author František Špaček
 */
public class ExceptionHandler {
    
    private static final Logger LOG = LoggerFactory.getLogger(
            ExceptionHandler.class);
    
    private final ObjectMapper mapper;
    
    public ExceptionHandler() {
        this.mapper = new ObjectMapper();
        setupHandler();
    }
    
    private void setupHandler() {
        exception(InternalServerErrorException.class, (e, req, resp) -> {
            LOG.error(HttpCode.ERROR.getMsg(), e);
            errorMessage(resp, HttpCode.ERROR);
        });
        
        exception(InvalidAuthenticationException.class, (e, req, resp) -> {
            LOG.warn(HttpCode.UNAUTHORIZED.getMsg(), e);
            errorMessage(resp, HttpCode.UNAUTHORIZED);
        });
        
        exception(InvalidInputException.class, (e, req, resp) -> {
            LOG.info(HttpCode.BAD_REQUEST.getMsg(), e);
            errorMessage(resp, HttpCode.BAD_REQUEST);
        });
        
        exception(InvalidCredentialsException.class, (e, req, resp) -> {
            LOG.warn(HttpCode.FORBIDDEN.getMsg(), e);
            errorMessage(resp, HttpCode.FORBIDDEN);
        });
    }
    
    private void errorMessage(Response resp, HttpCode code) {
        final ErrorDto error = new ErrorDto(code.getCode(), code.getMsg());
        resp.status(code.getCode());
        try {
            resp.type(ContentType.JSON.getValue());
            resp.body(mapper.writeValueAsString(error));
        } catch (JsonProcessingException ex) {
            LOG.error("Unable to serialize error message", ex);
        }
    }
    
    private static class ErrorDto {
        
        private final int code;
        private final String msg;
        
        public ErrorDto(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
        
        public int getCode() {
            return code;
        }
        
        public String getMsg() {
            return msg;
        }
    }
    
}
