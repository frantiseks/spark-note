/*
 * 
 */
package eu.devs.spark.note.exception;

/**
 *
 * @author František Špaček
 */
public class InvalidInputException extends RuntimeException {

    public InvalidInputException(String message) {
        super(message);
    }

    public InvalidInputException(String message, Throwable cause) {
        super(message, cause);
    }

}
