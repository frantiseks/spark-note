/*
 *
 */
package eu.devs.spark.note.exception;

/**
 * Exception is consider as wrapper for errors of persistence storage.
 *
 * @author František Špaček
 */
public class InternalStorageException extends Exception {

    public InternalStorageException(String message) {
        super(message);
    }

    public InternalStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
