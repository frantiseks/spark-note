/*
 *
 */
package eu.devs.spark.note.exception;

/**
 *
 * @author František Špaček
 */
public class InvalidAuthenticationException extends RuntimeException{

    public InvalidAuthenticationException(String message) {
        super(message);
    }

    public InvalidAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
}
