/*
 * 
 */
package eu.devs.spark.note.exception;

/**
 * Exception which is intended for serious server errors.
 *
 * @author František Špaček
 */
public class InternalServerErrorException extends Exception {

    public InternalServerErrorException(String message) {
        super(message);
    }

    public InternalServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }

}
