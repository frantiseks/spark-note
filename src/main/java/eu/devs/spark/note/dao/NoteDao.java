/*
 * 
 */
package eu.devs.spark.note.dao;

import eu.devs.spark.note.document.NoteDocument;
import eu.devs.spark.note.exception.InternalStorageException;

import java.util.List;

/**
 * Defines access to stored notes.
 *
 * @author František Špaček
 */
public interface NoteDao {

  NoteDocument getOneForUser(String uid, String noteId) throws InternalStorageException;

  List<NoteDocument> getAllForUser(String uid, String query) throws InternalStorageException;

  String createForUser(String uid, NoteDocument note) throws InternalStorageException;

  void updateForUser(String uid, String noteId, NoteDocument note)
      throws InternalStorageException;

  void deleteForUser(String uid, String noteId) throws InternalStorageException;

}
