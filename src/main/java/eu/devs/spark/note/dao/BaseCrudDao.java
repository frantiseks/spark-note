/*
 * 
 */
package eu.devs.spark.note.dao;

import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.eq;
import eu.devs.spark.note.document.BaseDocument;
import eu.devs.spark.note.exception.InternalStorageException;

/**
 * Base crud operations implementation.
 *
 * @author František Špaček
 * @param <TDocument> type of document
 */
public class BaseCrudDao< TDocument extends BaseDocument>
        extends BaseDao<TDocument> implements CrudDao<String, TDocument> {

    public BaseCrudDao(MongoCollection<TDocument> collection) {
        super(collection);
    }

    @Override
    public TDocument getOne(String id) throws InternalStorageException {
        return collection.find(eq("_id", id))
                .first();
    }

    @Override
    public String create(TDocument document) throws InternalStorageException {
        collection.insertOne(document);
        return document.getId();
    }

    @Override
    public void update(TDocument document) throws InternalStorageException {
        collection.findOneAndReplace(eq("_id", document.getId()), document);
    }

    @Override
    public void delete(String id) throws InternalStorageException {
        collection.findOneAndDelete(eq("_id", id));
    }
}
