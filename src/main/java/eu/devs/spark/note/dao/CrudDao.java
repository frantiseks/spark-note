/*
 */
package eu.devs.spark.note.dao;

import eu.devs.spark.note.exception.InternalStorageException;

/**
 * Defines generic DAO operations.
 *
 * @author František Špaček
 * @param <TId> class of unique id
 * @param <TDocument> class of document model
 */
public interface CrudDao<TId, TDocument> {

    TDocument getOne(TId id) throws InternalStorageException;

    TId create(TDocument document) throws InternalStorageException;

    void update(TDocument document) throws InternalStorageException;

    void delete(TId id) throws InternalStorageException;
}
