/*
 * 
 */
package eu.devs.spark.note.dao.impl;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

import com.mongodb.client.MongoCollection;

import eu.devs.spark.note.dao.BaseDao;
import eu.devs.spark.note.dao.NoteDao;
import eu.devs.spark.note.document.NoteDocument;
import eu.devs.spark.note.exception.InternalStorageException;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link NoteDao}.
 *
 * @author František Špaček
 */
public class NoteDaoImpl extends BaseDao<NoteDocument> implements NoteDao {

  /**
   * Creates instance with provided collection.
   *
   * @param collection note document collection
   */
  public NoteDaoImpl(MongoCollection<NoteDocument> collection) {
    super(collection);
  }

  @Override
  public NoteDocument getOneForUser(String uid, String noteId) throws InternalStorageException {
    return collection.find(and(eq("_id", noteId), eq("userId", uid))).first();
  }

  @Override
  public List<NoteDocument> getAllForUser(String uid, String query)
      throws InternalStorageException {
    return collection.find(eq("userId", uid)).into(new ArrayList<>());
  }

  @Override
  public String createForUser(String uid, NoteDocument note) throws InternalStorageException {
    note.setUserId(uid);
    collection.insertOne(note);
    return note.getId();
  }

  @Override
  public void updateForUser(String uid, String noteId, NoteDocument note)
      throws InternalStorageException {
    collection.findOneAndReplace(and(eq("_id", noteId), eq("userId", uid)), note);
  }

  @Override
  public void deleteForUser(String uid, String noteId) throws InternalStorageException {
    collection.findOneAndDelete(and(eq("_id", noteId), eq("userId", uid)));
  }

}
