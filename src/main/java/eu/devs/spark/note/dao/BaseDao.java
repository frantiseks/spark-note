/*
 */
package eu.devs.spark.note.dao;

import com.mongodb.client.MongoCollection;
import java.util.Objects;
import org.bson.codecs.configuration.CodecRegistry;

/**
 * Defines base DAO. For specified document type must be available serialization
 * codec and this codec must be register via {@link CodecRegistry};
 *
 * @author František Špaček
 * @param <T> Type of document
 */
public abstract class BaseDao<T> {

    protected final MongoCollection<T> collection;

    /**
     * Provides base constructor with specified typed collection.
     *
     * @param collection instance of collection
     * @throws NullPointerException if provided collection is {@code null}
     */
    public BaseDao(MongoCollection<T> collection) {
        this.collection = Objects.requireNonNull(collection,
                "collection cannot be null");
    }
}
