/*
 * 
 */
package eu.devs.spark.note.dao;

import eu.devs.spark.note.document.UserDocument;
import eu.devs.spark.note.exception.InternalStorageException;

/**
 * Defines access to stored user data.
 *
 * @author František Špaček
 */
public interface UserDao extends CrudDao<String, UserDocument> {

    String getIdByUsernameAndPassword(String username, String passwordHash)
            throws InternalStorageException;
}
