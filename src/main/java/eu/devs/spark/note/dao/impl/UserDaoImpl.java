/*
 */
package eu.devs.spark.note.dao.impl;

import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import eu.devs.spark.note.dao.BaseCrudDao;
import eu.devs.spark.note.dao.UserDao;
import eu.devs.spark.note.document.UserDocument;
import eu.devs.spark.note.exception.InternalStorageException;

/**
 * Implementation of {@link UserDao}.
 *
 * @author František Špaček
 */
public class UserDaoImpl extends BaseCrudDao<UserDocument>
        implements UserDao {

    /**
     * Creates instance with provided collection.
     *
     * @param collection user document collection
     */
    public UserDaoImpl(MongoCollection<UserDocument> collection) {
        super(collection);
    }

    @Override
    public String getIdByUsernameAndPassword(String username,
            String passwordHash) throws InternalStorageException {
        UserDocument user = collection.find(and(eq("username", username),
                eq("password", passwordHash)), UserDocument.class).first();

        return user == null ? "" : user.getId();
    }
}
