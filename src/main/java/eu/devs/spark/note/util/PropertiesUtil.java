/*
 *
 */
package eu.devs.spark.note.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Properties utility class.
 *
 * @author František Špaček
 */
public final class PropertiesUtil {

  /**
   * Utility pattern.
   */
  private PropertiesUtil() {
  }

  /**
   * Loads and merge properties from multiple streams.
   *
   * @param streams stream with properties
   * @return merged properties
   * @throws IOException if error during reading occurs.
   */
  public static Properties loadFrom(InputStream... streams)
      throws IOException {
    Properties properties = new Properties();
    for (InputStream stream : streams) {
      if (stream != null) {
        properties.load(stream);
      }
    }

    return properties;
  }
}
