/*
 * 
 */
package eu.devs.spark.note.util;

import static java.lang.String.format;

/**
 * Utility class for arguments checking.
 *
 * @author František Špaček
 */
public class AssertUtil {

    /**
     * Utility class pattern.
     */
    private AssertUtil() {

    }

    /**
     * Checks if provided object is not null.
     *
     * @param <T> type of checked object
     * @param object object to check
     * @param argName argument name for error message
     * @return checked object
     */
    public static <T> T checkNotNull(T object, String argName) {
        if (object == null) {
            throw new IllegalArgumentException(format("%s cannot be null",
                    argName));
        }

        return object;
    }

    public static String checkNotEmpty(String str, String argName) {
        String nullSafe = checkNotNull(str, argName);

        if (nullSafe.isEmpty()) {
            throw new IllegalArgumentException(format("%s must be non-empty",
                    argName));
        }

        return nullSafe;
    }
}
