/*
 *
 */
package eu.devs.spark.note.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Digest utility class.
 *
 * @author František Špaček
 */
public final class DigestUtil {

  private static final String SHA256 = "SHA-256";

  private DigestUtil() {

  }

  public static String sha256(String str) {
    try {
      MessageDigest md = MessageDigest.getInstance(SHA256);
      byte[] digest = md.digest(str.getBytes("UTF-8"));
      return hex(digest);
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
      throw new IllegalStateException("Unable to create digest", ex);
    }
  }

  private static String hex(byte[] a) {
    StringBuilder sb = new StringBuilder(a.length * 2);
    for (byte b : a) {
      sb.append(String.format("%02x", b & 0xff));
    }
    return sb.toString();
  }
}
