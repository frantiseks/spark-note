/*
 * 
 */
package eu.devs.spark.note.api.dto;

import java.util.Date;

/**
 * Defines DTO for note document.
 *
 * @author František Špaček
 */
public interface NoteDto {

    /**
     * Gets id of note.
     *
     * @return id of note never {@code null}.
     */
    String getId();

    String getTitle();

    String getContent();

    Date getCreated();

    Date getEdited();
}
