/*
 *
 */

package eu.devs.spark.note.api;

import eu.devs.spark.note.service.UserService;
import eu.devs.spark.note.util.AssertUtil;
import spark.ResponseTransformer;

/**
 * Resource for user.
 *
 * @author František Špaček
 */
public class UserResource {

  private static final String RESOURCE_NAME = "/api/user";

  private final ResponseTransformer responseTransformer;
  private final UserService userService;

  public UserResource(UserService noteService, ResponseTransformer responseTransformer) {
    this.userService = AssertUtil.checkNotNull(noteService, "noteService");
    this.responseTransformer = AssertUtil.checkNotNull(responseTransformer, "responseTransformer");
  }
  
  private void setupEndpoints(){
  }

}
