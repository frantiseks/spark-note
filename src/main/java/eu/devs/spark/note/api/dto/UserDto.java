/*
 */

package eu.devs.spark.note.api.dto;

/**
 * User DTO definition.
 *
 * @author František Špaček
 */
public interface UserDto {

  String getUsername();

  String getFirstname();

  String getLastname();

  String getEmail();
}
