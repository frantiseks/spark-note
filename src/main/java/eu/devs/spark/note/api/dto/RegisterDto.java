/*
 * 
 */
package eu.devs.spark.note.api.dto;

/**
 *
 * @author František Špaček
 */
public interface RegisterDto {

    String getUsername();

    String getPassword();

    String getFirstname();

    String getLastname();

    String getEmail();
}
