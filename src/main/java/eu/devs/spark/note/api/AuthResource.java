/*
 *
 */
package eu.devs.spark.note.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.mrbean.MrBeanModule;
import eu.devs.spark.note.api.dto.AuthDto;
import eu.devs.spark.note.api.dto.RegisterDto;
import eu.devs.spark.note.auth.Authenticator;
import eu.devs.spark.note.exception.InternalServerErrorException;
import eu.devs.spark.note.exception.InvalidCredentialsException;
import java.io.IOException;
import static java.lang.String.format;
import spark.Request;
import spark.Response;
import static spark.Spark.post;

/**
 * Authentication resource.
 *
 * @author František Špaček
 */
public class AuthResource {
    
    private static final String AUTH_HEADER = "Authorization";
    private static final String AUTH_HEADER_VALUE = "Bearer %s";
    private final ObjectMapper objectMapper;
    private final Authenticator authenticator;
    
    public AuthResource(Authenticator authenticator) {
        this.authenticator = authenticator;
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new MrBeanModule());
        setupEndpoint();
    }
    
    private void setupEndpoint() {
        post("/auth", (req, resp) -> authEndpoint(req, resp));
        post("/register", (req, resp) -> registerEndpoint(req, resp));
    }
    
    private Object authEndpoint(Request req, Response resp)
            throws IllegalArgumentException, InvalidCredentialsException,
            InternalServerErrorException {
        final AuthDto dto;
        try {
            dto = objectMapper.readValue(req.body(), AuthDto.class);
        } catch (IOException ex) {
            throw new IllegalArgumentException("Invalid payload", ex);
        }
        
        String token = authenticator.getTokenFor(dto.getUsername(),
                dto.getPassword());
        resp.header(AUTH_HEADER, format(AUTH_HEADER_VALUE, token));
        resp.body("");
        
        return resp;
    }
    
    private Object registerEndpoint(Request req, Response resp)
            throws IOException, InternalServerErrorException {
        RegisterDto dto = objectMapper.readValue(req.body(),
                RegisterDto.class);
        authenticator.registerUser(dto);
        resp.body("");
        return resp;
    }
    
}
