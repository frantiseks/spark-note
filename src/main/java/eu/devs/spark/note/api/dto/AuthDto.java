/*
 * 
 */
package eu.devs.spark.note.api.dto;

/**
 *
 * @author František Špaček
 */
public interface AuthDto {

    String getUsername();

    String getPassword();
}
