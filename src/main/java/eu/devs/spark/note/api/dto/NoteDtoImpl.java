/*
 * 
 */
package eu.devs.spark.note.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import eu.devs.spark.note.api.dto.NoteDto;
import eu.devs.spark.note.document.NoteDocument;
import eu.devs.spark.note.util.AssertUtil;
import java.util.Date;

/**
 * Immutable implementation of {@link NoteDto}.
 *
 * @author František Špaček
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NoteDtoImpl implements NoteDto {

    private final String id;
    private final String title;
    private final String content;
    private final Date created;
    private final Date edited;

    /**
     * Creates instance from provided arguments.
     *
     * @param id id of note
     * @param title title of note
     * @param content content of note
     * @param created date of note creation
     * @param edited last edit date of note
     * @throws IllegalArgumentException if required argument is not provided or
     * is invalid.
     */
    public NoteDtoImpl(String id, String title, String content, Date created,
            Date edited) {
        this.id = AssertUtil.checkNotEmpty(id, "id");
        this.title = title;
        this.content = content;
        this.created = created;
        this.edited = edited;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public Date getEdited() {
        return edited;
    }

    /**
     * Converts document to DTO.
     *
     * @param document document to convert
     * @return converted document
     * @throws IllegalArgumentException if {@code null} provided;
     */
    public static NoteDto from(NoteDocument document) {
        AssertUtil.checkNotNull(document, "document");
        return new NoteDtoImpl(document.getId(), document.getTitle(),
                document.getContent(), document.getCreated(),
                document.getEdited());
    }

    public static NoteDto from(String id) {
        return new NoteDtoImpl(id, null, null, null, null);
    }

    public static NoteDocument toDocument(NoteDto dto) {
        AssertUtil.checkNotNull(dto, "dto");
        NoteDocument document = new NoteDocument();
        document.setId(dto.getId());
        document.setTitle(dto.getTitle());
        document.setContent(dto.getContent());
        return document;
    }

    @Override
    public String toString() {
        return "NoteDtoImpl{" + "id=" + id + ", title=" + title + '}';
    }

}
