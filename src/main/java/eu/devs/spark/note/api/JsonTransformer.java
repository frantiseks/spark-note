/*
 * 
 */
package eu.devs.spark.note.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import spark.ResponseTransformer;

/**
 * Implementation of {@link ResponseTransformer}.
 *
 * @author František Špaček
 */
public class JsonTransformer implements ResponseTransformer {

    private final ObjectMapper objectMapper;

    public JsonTransformer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
    
    @Override
    public String render(Object model) throws Exception {
        return objectMapper.writeValueAsString(model);
    }
}
