/*
 * 
 */
package eu.devs.spark.note.api;

import static spark.Spark.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.mrbean.MrBeanModule;

import eu.devs.spark.note.api.dto.NoteDto;
import eu.devs.spark.note.api.dto.NoteDtoImpl;
import eu.devs.spark.note.auth.RequestContext;
import eu.devs.spark.note.enums.HttpCode;
import eu.devs.spark.note.exception.InternalServerErrorException;
import eu.devs.spark.note.exception.InvalidInputException;
import eu.devs.spark.note.service.NoteService;
import eu.devs.spark.note.util.AssertUtil;
import spark.Request;
import spark.Response;
import spark.ResponseTransformer;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * REST resource for notes.
 *
 * @author František Špaček
 */
public class NoteResource {

  private static final String RESOURCE_NAME = "/api/notes";

  private final ResponseTransformer responseTransformer;
  private final NoteService noteService;
  private final ObjectMapper objectMapper;

  /**
   * Creates instance for provided service
   *
   * @param noteService instance of note service
   * @param transformer response transformer
   * @throws NullPointerException if provided argument is {@code null}
   */
  public NoteResource(NoteService noteService,
      ResponseTransformer transformer) {
    this.noteService = Objects.requireNonNull(noteService,
        "noteService cannot be null");
    this.responseTransformer = Objects.requireNonNull(transformer,
        "transformer cannot be null");
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new MrBeanModule());
    setupEndpoints();
  }

  private void setupEndpoints() {
    get(RESOURCE_NAME, (req, resp) -> getAllEndpoint(req), responseTransformer);
    get(RESOURCE_NAME + "/:id", (req, resp) -> getOneEndpoint(req), responseTransformer);
    post(RESOURCE_NAME, (req, resp) -> createEndpoint(req), responseTransformer);
    put(RESOURCE_NAME + "/:id", (req, resp) -> updateEndpoint(req, resp));

    delete(RESOURCE_NAME + "/:id", (req, resp) -> deleteEndpoint(req, resp));
  }

  private List<NoteDto> getAllEndpoint(Request req) throws InternalServerErrorException {
    String uid = RequestContext.get().getUid();
    return noteService.getAllForUser(uid, req.queryParams("q"))
        .stream()
        .map(NoteDtoImpl::from)
        .collect(Collectors.toList());
  }

  private NoteDto createEndpoint(Request req) throws InvalidInputException,
      InternalServerErrorException {
    String uid = RequestContext.get().getUid();
    final NoteDto dto;
    try {
      dto = objectMapper.readValue(req.body(), NoteDto.class);
    } catch (IOException ex) {
      throw new InvalidInputException("Unable to deserialize payloed", ex);
    }
    String id = noteService.createNoteForUser(uid, NoteDtoImpl.toDocument(dto));
    return NoteDtoImpl.from(id);
  }

  private NoteDto getOneEndpoint(Request req) throws InternalServerErrorException {
    String uid = RequestContext.get().getUid();
    return NoteDtoImpl.from(noteService.getForUser(uid, req.params("id")));
  }

  private Object deleteEndpoint(Request req, Response resp) throws InternalServerErrorException {
    String id = AssertUtil.checkNotEmpty(req.params("id"), "id");
    String uid = RequestContext.get().getUid();
    noteService.deleteNoteForUser(uid, id);
    resp.status(HttpCode.NO_CONTENT.getCode());
    return resp;
  }

  private Object updateEndpoint(Request req, Response resp) throws InvalidInputException,
      InternalServerErrorException {
    String id = AssertUtil.checkNotEmpty(req.params("id"), "id");
    String uid = RequestContext.get().getUid();
    final NoteDto dto;
    try {
      dto = objectMapper.readValue(req.body(), NoteDto.class);
    } catch (IOException ex) {
      throw new InvalidInputException("Unable to deserialize payloed", ex);
    }
    noteService.updateNoteForUser(uid, id, NoteDtoImpl.toDocument(dto));

    resp.status(HttpCode.NO_CONTENT.getCode());
    return resp;
  }

}
