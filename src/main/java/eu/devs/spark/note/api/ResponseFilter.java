/*
 */
package eu.devs.spark.note.api;


import static spark.Spark.after;

import eu.devs.spark.note.auth.RequestContext;
import eu.devs.spark.note.enums.ContentType;

/**
 * Setups response filter.
 *
 * @author František Špaček
 */
public class ResponseFilter {

  public ResponseFilter() {
    setupFilters();
  }

  private void setupFilters() {
    after((req, resp) -> {
      resp.type(ContentType.JSON.getValue());
      RequestContext.clear();
    });
  }
}
