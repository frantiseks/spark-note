/*
 * 
 */
package eu.devs.spark.note;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;

import eu.devs.spark.note.api.AuthResource;
import eu.devs.spark.note.api.JsonTransformer;
import eu.devs.spark.note.api.NoteResource;
import eu.devs.spark.note.api.ResponseFilter;
import eu.devs.spark.note.auth.AuthenticationFilter;
import eu.devs.spark.note.auth.Authenticator;
import eu.devs.spark.note.auth.JwtAuthenticator;
import eu.devs.spark.note.dao.NoteDao;
import eu.devs.spark.note.dao.UserDao;
import eu.devs.spark.note.dao.impl.NoteDaoImpl;
import eu.devs.spark.note.dao.impl.UserDaoImpl;
import eu.devs.spark.note.document.NoteDocument;
import eu.devs.spark.note.document.UserDocument;
import eu.devs.spark.note.document.codec.NoteCodec;
import eu.devs.spark.note.document.codec.UserCodec;
import eu.devs.spark.note.exception.ExceptionHandler;
import eu.devs.spark.note.service.NoteService;
import eu.devs.spark.note.service.UserService;
import eu.devs.spark.note.service.impl.NoteServiceImpl;
import eu.devs.spark.note.service.impl.UserServiceImpl;
import eu.devs.spark.note.util.PropertiesUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Application runner.
 *
 * @author František Špaček
 */
public class ApplicationRunner {
  
  private static final String DEFAULT_PROPERTIES = "/application.properties";
  private static final String USER_COLLECTION = "users";
  private static final String NOTE_COLLECTION = "notes";

  /**
   * Main method of the application.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    final Properties properties = readProperties();
    final String mongoHost = properties.getProperty("mongo.host");
    final String mongoDatabaseName = properties.getProperty(
        "mongo.database");
    final String signingKey = properties.getProperty("auth.signing.key");
    
    final MongoClient mongoClient = initializeMongoClient(mongoHost);
    
    final MongoDatabase mongoDatabase = mongoClient.getDatabase(
        mongoDatabaseName);
    
    final MongoCollection<NoteDocument> noteCollection = mongoDatabase
        .getCollection(NOTE_COLLECTION, NoteDocument.class);
    createNoteResource(noteCollection);
    
    final MongoCollection<UserDocument> userCollection = mongoDatabase
        .getCollection(USER_COLLECTION, UserDocument.class);
    
    final UserService userService = createUserService(userCollection);
    final Authenticator authenticator = new JwtAuthenticator(userService,
        signingKey);
    
    createAuthFilter(authenticator);
    createAuthResource(authenticator);
    createResponseFilters();
    createExceptionHandler();
    
    addShutdownHook(mongoClient);
  }
  
  private static NoteResource createNoteResource(
      MongoCollection<NoteDocument> collection) {
    final NoteDao dao = new NoteDaoImpl(collection);
    final NoteService service = new NoteServiceImpl(dao);
    
    return new NoteResource(service,
        new JsonTransformer(new ObjectMapper()));
  }
  
  private static ResponseFilter createResponseFilters() {
    return new ResponseFilter();
  }
  
  private static AuthenticationFilter createAuthFilter(
      Authenticator authenticator) {
    return new AuthenticationFilter(authenticator);
  }
  
  private static AuthResource createAuthResource(
      Authenticator authenticator) {
    return new AuthResource(authenticator);
  }
  
  private static UserService createUserService(
      MongoCollection<UserDocument> userCollection) {
    final UserDao dao = new UserDaoImpl(userCollection);
    return new UserServiceImpl(dao);
  }
  
  private static ExceptionHandler createExceptionHandler() {
    return new ExceptionHandler();
  }
  
  private static void addShutdownHook(final MongoClient client) {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        client.close();
      }
    });
  }
  
  private static Properties readProperties() {
    InputStream defaultProperties = ApplicationRunner.class
        .getResourceAsStream(DEFAULT_PROPERTIES);
    try {
      return PropertiesUtil.loadFrom(defaultProperties);
    } catch (IOException ex) {
      throw new Error("Unable to load default properties", ex);
    }
  }
  
  private static MongoClient initializeMongoClient(String mongoHost) {
    
    CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
        MongoClient.getDefaultCodecRegistry(),
        CodecRegistries.fromCodecs(new NoteCodec(), new UserCodec()));
    
    MongoClientOptions options
        = MongoClientOptions.builder()
        .codecRegistry(codecRegistry)
        .build();
    
    return new MongoClient(mongoHost, options);
  }
  
}
