/*
 * 
 */
package eu.devs.spark.note.service;

import eu.devs.spark.note.document.UserDocument;
import eu.devs.spark.note.exception.InternalServerErrorException;

/**
 * Defines access to stored users.
 *
 * @author František Špaček
 */
public interface UserService {

  /**
   * Checks if user is presented in the system.
   *
   * @param id
   * @return
   * @throws InternalServerErrorException
   */
  String isValid(String id) throws InternalServerErrorException;

  /**
   * Registers new user to the system.
   *
   * @param user user data
   * @throws InternalServerErrorException if error occurs
   */
  void registerUser(UserDocument user) throws InternalServerErrorException;

  /**
   * Authenticates user with provided username and password.
   *
   * @param username username
   * @param password password of user
   * @return {@code true} if user is presented and provided username and password is valid,
   *         otherwise {@code false}
   * @throws InternalServerErrorException if error occurs
   */
  String getUserId(String username, String password)
      throws InternalServerErrorException;

  UserDocument getUserInfo(String uid)  throws InternalServerErrorException;

  /**
   * Updates user by specified ID.
   *
   * @param id   user id
   * @param user user data to be updated
   * @throws InternalServerErrorException if error occurs
   *
   */
  void updateUser(String id, UserDocument user)
      throws InternalServerErrorException;

  /**
   * Deletes user with provided ID.
   *
   * @param id user id
   * @throws InternalServerErrorException if error occurs
   */
  void deleteUser(String id) throws InternalServerErrorException;

}
