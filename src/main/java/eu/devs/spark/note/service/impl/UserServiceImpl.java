/*
 * 
 */
package eu.devs.spark.note.service.impl;

import eu.devs.spark.note.dao.UserDao;
import eu.devs.spark.note.document.UserDocument;
import eu.devs.spark.note.exception.InternalServerErrorException;
import eu.devs.spark.note.exception.InternalStorageException;
import eu.devs.spark.note.service.UserService;
import eu.devs.spark.note.util.AssertUtil;
import eu.devs.spark.note.util.DigestUtil;

/**
 * Implementation of {@link UserService}.
 *
 * @author František Špaček
 */
public class UserServiceImpl implements UserService {

  private final UserDao dao;

  /**
   * Creates instance for provided user DAO.
   *
   * @param dao
   * @throws IllegalArgumentException if provided DAO is {@code null}
   */
  public UserServiceImpl(UserDao dao) {
    this.dao = AssertUtil.checkNotNull(dao, "dao");
  }

  @Override
  public String isValid(String id) throws InternalServerErrorException {
    try {
      UserDocument user = dao.getOne(id);
      return user != null ? user.getId() : null;
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to fetch user", ex);
    }
  }

  @Override
  public void registerUser(UserDocument user)
      throws InternalServerErrorException {
    try {
      dao.create(user);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to register user",
          ex);
    }
  }

  @Override
  public String getUserId(String username, String password)
      throws InternalServerErrorException {
    try {
      final String hash = DigestUtil.sha256(password);
      return dao.getIdByUsernameAndPassword(username, hash);
    } catch (InternalStorageException | IllegalStateException ex) {
      throw new InternalServerErrorException("Unable to authenticate "
          + "user", ex);
    }
  }

  @Override
  public UserDocument getUserInfo(String uid)
      throws InternalServerErrorException {
    try {
      return dao.getOne(uid);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to retrieve user", ex);
    }
  }

  @Override
  public void updateUser(String id, UserDocument user)
      throws InternalServerErrorException {
    try {
      user.setId(id);
      dao.update(user);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to update user", ex);
    }
  }

  @Override
  public void deleteUser(String id) throws InternalServerErrorException {
    try {
      dao.delete(id);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to delete user", ex);
    }
  }

}
