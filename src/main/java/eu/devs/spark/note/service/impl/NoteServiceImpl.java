/*
 * 
 */
package eu.devs.spark.note.service.impl;

import static java.lang.String.format;

import eu.devs.spark.note.dao.NoteDao;
import eu.devs.spark.note.document.NoteDocument;
import eu.devs.spark.note.exception.InternalServerErrorException;
import eu.devs.spark.note.exception.InternalStorageException;
import eu.devs.spark.note.service.NoteService;
import eu.devs.spark.note.util.AssertUtil;

import java.util.Date;
import java.util.List;

/**
 * Implementation of {@link NoteService}.
 *
 * @author František Špaček
 */
public class NoteServiceImpl implements NoteService {

  private final NoteDao dao;

  public NoteServiceImpl(NoteDao dao) {
    this.dao = dao;
  }

  @Override
  public NoteDocument getForUser(String uid, String noteId) throws InternalServerErrorException {
    AssertUtil.checkNotEmpty(uid, "uid");
    AssertUtil.checkNotEmpty(noteId, "noteId");
    try {
      return dao.getOneForUser(uid, noteId);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException(format("Unable to fetch "
          + "note for user %s with id %s", uid, noteId), ex);
    }
  }

  @Override
  public List<NoteDocument> getAllForUser(String uid, String query)
      throws InternalServerErrorException {
    AssertUtil.checkNotEmpty(uid, "uid");
    try {
      return dao.getAllForUser(uid, nullSafeString(query));
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to fetch notes",
          ex);
    }
  }

  @Override
  public String createNoteForUser(String uid, NoteDocument note)
      throws InternalServerErrorException {
    AssertUtil.checkNotEmpty(uid, "uid");
    NoteDocument document = AssertUtil.checkNotNull(note, "note");
    document.setCreated(new Date());
    document.setEdited(new Date());
    try {
      return dao.createForUser(uid, document);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to create note",
          ex);
    }
  }

  @Override
  public void updateNoteForUser(String uid, String noteId, NoteDocument note)
      throws InternalServerErrorException {
    AssertUtil.checkNotEmpty(uid, "uid");
    AssertUtil.checkNotEmpty(noteId, "noteId");
    NoteDocument document = AssertUtil.checkNotNull(note, "note");
    document.setId(noteId);
    document.setUserId(uid);
    document.setEdited(new Date());
    try {
      dao.updateForUser(uid, noteId, document);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to update note",
          ex);
    }
  }

  @Override
  public void deleteNoteForUser(String uid, String noteId)
      throws InternalServerErrorException {
    AssertUtil.checkNotEmpty(uid, "uid");
    AssertUtil.checkNotEmpty(noteId, "noteId");
    try {
      dao.deleteForUser(uid, noteId);
    } catch (InternalStorageException ex) {
      throw new InternalServerErrorException("Unable to update note",
          ex);
    }
  }

  private String nullSafeString(String str) {
    return str == null ? "" : str;
  }

}
