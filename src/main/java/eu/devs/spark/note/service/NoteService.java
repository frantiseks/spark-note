/*
 *
 */
package eu.devs.spark.note.service;

import eu.devs.spark.note.document.NoteDocument;
import eu.devs.spark.note.exception.InternalServerErrorException;

import java.util.List;

/**
 * Defines business logic access to notes.
 *
 * @author František Špaček
 */
public interface NoteService {

  /**
   * Gets note by specified id.
   *
   * @param uid
   * @param noteId
   * @return note if found, otherwise {@code null}.
   * @throws IllegalArgumentException     if provided id is {@code null}
   * @throws InternalServerErrorException if error occurs.
   */
  NoteDocument getForUser(String uid, String noteId) throws InternalServerErrorException;

  /**
   * Gets all stored notes filtered with specified query.
   *
   * @param uid
   * @param query filter query
   * @return list of stored notes, never {@code null}.
   * @throws InternalServerErrorException if error occurs.
   */
  List<NoteDocument> getAllForUser(String uid, String query) throws InternalServerErrorException;

  /**
   * Creates note from specified argument
   *
   * @param uid
   * @param note note representation
   * @return id of created note in persistence storage.
   * @throws IllegalArgumentException     if provided note is {@code null}
   * @throws InternalServerErrorException if error occurs.
   */
  String createNoteForUser(String uid, NoteDocument note) throws InternalServerErrorException;

  /**
   * Updates specified note.
   *
   * @param uid
   * @param noteId
   * @param note   note which update should be stored to persistence storage
   * @throws IllegalArgumentException     if provided note is {@code null}
   * @throws InternalServerErrorException if error occurs.
   */
  void updateNoteForUser(String uid, String noteId, NoteDocument note)
      throws InternalServerErrorException;

  /**
   * Deletes note with specified id.
   *
   * @param uid
   * @param noteId id of note
   * @throws IllegalArgumentException     if provided id is {@code null}
   * @throws InternalServerErrorException if error occurs.
   */
  void deleteNoteForUser(String uid, String noteId) throws InternalServerErrorException;
}
