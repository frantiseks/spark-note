/*
 * 
 */
package eu.devs.spark.note.enums;

/**
 * Enumeration of HTTP codes.
 *
 * @author František Špaček
 */
public enum HttpCode {

    OK(200, ""),
    NO_CONTENT(204, ""),
    BAD_REQUEST(400, "Wrong request"),
    UNAUTHORIZED(401, "Unauthorized"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Resource not found"),
    ERROR(500, "Internal server error");

    private final int code;
    private final String msg;

    private HttpCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
