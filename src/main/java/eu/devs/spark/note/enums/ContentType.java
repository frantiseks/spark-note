/*
 * 
 */
package eu.devs.spark.note.enums;

/**
 *
 * @author František Špaček
 */
public enum ContentType {

    JSON("application/json;charset=utf8");

    private final String value;

    private ContentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
