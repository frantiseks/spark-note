/*
 * 
 */
package eu.devs.spark.note.dao;

import com.mongodb.client.MongoCollection;
import eu.devs.spark.note.util.DigestUtil;
import org.junit.Test;
import static org.mockito.Mockito.mock;

/**
 * Tests for {@link BaseDao}.
 *
 * @author František Špaček;
 */
public class BaseDaoTest {

    @Test
    public void testConstructor_collection() {
        System.out.println("constructor_collection");
        BaseDao<Object> dao = new SimpleDao(mock(MongoCollection.class));
        System.out.println(DigestUtil.sha256("test"));
    }

    @Test(expected = NullPointerException.class)
    public void testConstructor_nullCollection() {
        System.out.println("constructor_nullCollection");
        BaseDao<Object> dao = new SimpleDao(null);
        
    }

    private class SimpleDao extends BaseDao<Object> {

        public SimpleDao(MongoCollection<Object> collection) {
            super(collection);
        }
    }
}
